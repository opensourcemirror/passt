# SPDX-License-Identifier: GPL-2.0-or-later
#
# PASST - Plug A Simple Socket Transport
#  for qemu/UNIX domain socket mode
#
# PASTA - Pack A Subtle Tap Abstraction
#  for network namespace/tap device mode
#
# test/migrate/rampstream_out - Check sequence correctness with outbound ramp
#
# Copyright (c) 2025 Red Hat
# Author: David Gibson <david@gibson.dropbear.id.au>

g1tools	ip jq dhclient socat cat
htools	ip jq

set	MAP_HOST4 192.0.2.1
set	MAP_HOST6 2001:db8:9a55::1
set	MAP_NS4 192.0.2.2
set	MAP_NS6 2001:db8:9a55::2
set	RAMPS 6000000

test	Interface name
g1out	IFNAME1 ip -j link show | jq -rM '.[] | select(.link_type == "ether").ifname'
hout	HOST_IFNAME ip -j -4 route show|jq -rM '[.[] | select(.dst == "default").dev] | .[0]'
hout	HOST_IFNAME6 ip -j -6 route show|jq -rM '[.[] | select(.dst == "default").dev] | .[0]'
check	[ -n "__IFNAME1__" ]

test	DHCP: address
guest1	ip link set dev __IFNAME1__ up
guest1	/sbin/dhclient -4 __IFNAME1__
g1out	ADDR1 ip -j -4 addr show|jq -rM '.[] | select(.ifname == "__IFNAME1__").addr_info[0].local'
hout	HOST_ADDR ip -j -4 addr show|jq -rM '.[] | select(.ifname == "__HOST_IFNAME__").addr_info[0].local'
check	[ "__ADDR1__" = "__HOST_ADDR__" ]

test	DHCPv6: address
# Link is up now, wait for DAD to complete
guest1	while ip -j -6 addr show tentative | jq -e '.[].addr_info'; do sleep 0.1; done
guest1	/sbin/dhclient -6 __IFNAME1__
# Wait for DAD to complete on the DHCP address
guest1	while ip -j -6 addr show tentative | jq -e '.[].addr_info'; do sleep 0.1; done
g1out	ADDR1_6 ip -j -6 addr show|jq -rM '[.[] | select(.ifname == "__IFNAME1__").addr_info[] | select(.prefixlen == 128).local] | .[0]'
hout	HOST_ADDR6 ip -j -6 addr show|jq -rM '[.[] | select(.ifname == "__HOST_IFNAME6__").addr_info[] | select(.scope == "global" and .deprecated != true).local] | .[0]'
check	[ "__ADDR1_6__" = "__HOST_ADDR6__" ]

test	TCP/IPv4: sequence check, ramps, outbound
g1out	GW1 ip -j -4 route show|jq -rM '.[] | select(.dst == "default").gateway'
hostb	socat -u TCP4-LISTEN:10006 EXEC:"test/rampstream check __RAMPS__"
sleep	1
guest1b	socat -u EXEC:"rampstream send __RAMPS__" TCP4:__MAP_HOST4__:10006
sleep	1

mon	echo "migrate tcp:0:20005" | socat -u STDIN UNIX:__STATESETUP__/qemu_1_mon.sock

hostw
